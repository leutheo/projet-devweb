<?php
    include_once'./includes/functions/contact-fonctions.php';
    include_once'./includes/parts/header.php';
    include_once'./includes/search-header.php';

    if (!empty($errors)) {
        var_dump($errors);
    }
?>  
    </section>
    <h1 class="title is-1">Contactez-nous ! </h1>
        <div class="container is-fluid">
            <form action="/contact.php" method="POST">
                <input placeholder="nom" type="text" name="nom" value="<?php form_values("nom") ?>"></br></br>
                <input placeholder="prenom" type="text" name="prenom" value="<?php form_values("prenom") ?>"></br></br>
                <input placeholder="courriel" type="email" name="courriel" value="<?php form_values("courriel") ?>"></br></br>
                <textarea placeholder="Saisisser votre texte ici" name="contact" id="contact" cols="30" rows="10" value="<?php form_values("contact") ?>"></textarea></br></br>
                <input type="submit" value="Soumettre votre demande">
            </form>
        </div>
    </section>

<?php 
    if (isset($errors) && !empty($errors)) {
?>
    <section>
        <h1 class="subtile is-2">Erreurs dans votre formulaire</h1>
        <div> 
            <?php
                foreach ($errors as $error) {
                    echo "<div class='error'>". $errors . "</div>";
                }
            ?>
        </div>
    </section>
<?php
}
    include_once'./includes/parts/footer.php';
?>