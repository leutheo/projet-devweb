<?php
    $errors = null;
    
    function form_values($nom){
        echo (isset($_POST[$nom]) ? $_POST[$nom] : "");
    }


    function validate_contact(){
        $errors = [];
        $errors["nom"] = (empty($_POST["nom"]) ? "Le nom ne peut pas être vide" : "") ."</br>";
        $errors["prenom"] = (empty($_POST["prenom"]) ? "Le prenom ne peut pas être vide" : "") ."</br>";
        $errors["courriel"] = (empty(validate_email($_POST["courriel"])) ? "Le courriel ne peut pas être vide et doit être formaté correctement" : "") ."</br>";
        $errors["contact"] = (strlen($_POST["contact"]) < 3 ? "Le message doit contenir plus de 3 caractères" : "");
        if (count($errors) >= 1) {
            return $errors;
        }
    }

    function validate_email($courriel){
        return filter_var($courriel, FILTER_VALIDATE_EMAIL);
    }

    function email_validation($str){
        return(!preg_match(
            "^[a-z0-9-]+(\.[a-z0-9-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}$^", $str)) ? false : true;
    }

    if ($_SERVER["RESQUEST_METHOD"] == "POST") {
        $form = validate_contact();
        if (count($form) > 1 ) {
            $errors = $form;
        }
        return $errors;
    }

?>