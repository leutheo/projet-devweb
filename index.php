<?php 
    include_once'./includes/parts/header.php';
    include_once'./includes/search-header.php';

    $variable = "Comment en anglais";

    $utilisateur = [
        ["nom"=> "Waganwheel", "prenom"=>"Jam", "comment"=>"c'est hat les tableaux"],
        ["nom"=> "Waganwheel", "prenom"=>"Jim", "comment"=>"c'est hit les tableaux"],
        ["nom"=> "Waganwheel", "prenom"=>"Jom", "comment"=>"c'est hot les tableaux"],
        ["nom"=> "Waganwheel", "prenom"=>"Jem", "comment"=>"c'est het les tableaux"],
        ["nom"=> "Waganwheel", "prenom"=>"Jum", "comment"=>"c'est hut les tableaux"],
        ["nom"=> "Waganwheel", "prenom"=>"Jym", "comment"=>"c'est hyt les tableaux"]
    ];

    include_once'./includes/comment/form-comment.php';
    
    ?>
    <section class="hero">
    <div class="hero-body">
        <div class="container is-fluid">
            <h1 class="title"><?php echo $variable; ?></h1>
        </div>
    </div>
    </section>

    </section>
        <div class="container is-fluid">
            <?php 
                foreach ($utilisateur as $cle => $user) {
            ?>
                <h5 class="subtitle is-5">Nom complet: 
                    <?php echo $user["prenom"];?> 
                    <?php echo $user["nom"]; ?>
                </h5>
                <h6 class="subtitle is-6">Commentaire:</h6>
                <section>
                    <?php 
                        echo $user["comment"];
                    ?>
                </section>
            <?php 
                }
            ?>        

            <section>
                Enim mollitia quas non. Deleniti quos similique et et et fugit. Sint voluptas qui incidunt.
                Sint eum et placeat quia provident explicabo velit. Id aut voluptas iure dolores sed. Incidunt aut non nemo officia cum mollitia iure. Nihil quos quaerat sapiente enim quas sunt eos. Eum soluta perferendis et recusandae omnis.
                Quia est iusto aut quasi impedit. Omnis et consequuntur excepturi vel. Velit illum corporis molestiae minima qui sit. Aut nobis quia quod libero nesciunt est dolore in. Doloribus quis voluptate quod. Tempore veniam sequi sit enim.
            </section>
        </div>
    </section>
    <?php
        include_once'./includes/parts/footer.php';
?>